var gulp       = require("gulp");
var livereload = require("gulp-livereload");
var minify     = require("gulp-clean-css");
var prefixer   = require("gulp-autoprefixer");
var concat     = require("gulp-concat");
var less       = require("gulp-less");

// JS Task
gulp.task("js", function() {

  return gulp.src("src/public/js/main.js")
             .pipe(livereload());

});

// CSS Task
gulp.task("css", function() {

  var srcDir  = "./src/public/styles/less";
  var distDir = "./src/public/styles";

  return gulp.src(srcDir + "/includes.less")
             .pipe(less())
             .pipe(prefixer("last 2 versions"))
             .pipe(minify({ keepBreaks: false }))
             .pipe(concat("main.css"))
             .pipe(livereload())
             .pipe(gulp.dest(distDir));

});

// Watch Task
gulp.task("watch", function() {

  livereload.listen();
  gulp.watch(["src/public/templates/**/*.html"]).on("change", livereload.changed);
  gulp.watch(["src/public/templates/**/*.html.twig"]).on("change", livereload.changed);
  gulp.watch('src/public/js/**/*', ["js"]);
  gulp.watch('src/public/styles/**/*', ["css"]);

});

// Default Task
gulp.task("default", ["watch", "css"]);
