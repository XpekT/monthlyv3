# Monthly V3    

A daily expense/income tracker written in PHP with the slim framework and Laravel's Eloquent ORM.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/) 