<?php

require __DIR__ . "/../../vendor/autoload.php";

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use App\Classes\Database;

$db = new Database;
$db->start();

//Creating schema
Capsule::schema()->create("test", function (Blueprint $table) {
    $table->increments("id");
    $table->string("username");
    $table->timestamps();
});
