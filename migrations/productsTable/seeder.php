<?php

require __DIR__ . "/../../vendor/autoload.php";

use App\Models\Product;
use App\Classes\Database;
use Stringy\StaticStringy as Stringy;

date_default_timezone_set("Europe/Lisbon");

$db = new Database;
$db->start();

$faker = Faker\Factory::create();

if(isset($argv[1])) {
  $runs = $argv[1];
}

for ($i = 0; $i < $runs; $i++) {

  $name = $faker->word;

  Product::create([
    "user"       => $faker->randomElement(["Bruno", "Ana", "Mikaz"]),
    "name"       => $name,
    "price"      => $faker->randomFloat($nbMaxDecimals = 2, $min = 0.5, $max = 1000),
    "type"       => $faker->numberBetween($min = 1, $max = 14),
    "slug"       => Stringy::slugify($name),
    "created_at" => $faker->dateTimeThisDecade($max = "now")
  ]);

}

echo $runs . " were added to the database!";
