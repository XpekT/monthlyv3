new Vue({
  el: "#app",
  data: {
    index: 8,
    allLoaded: false,
    loading: false,
    total: 0,
    modal: {
      showModal: false,
      elements: []
    },
    navbar: {
      active: ""
    },
    types: [],
    loadedProducts: []
  },
  beforeCreate: function() {

    // Resets the locatStorage link to whatever page the user lands
    var currentLocation = window.location.pathname;
    var link = currentLocation.replace("/", "");

    localStorage.setItem("link", link);

    // Get the total products on the database
    this.$http.get("/products/api/total").then(function success(response) {
      this.total = response.data;
    });

  },
  created: function() {

    this.$http.get("/products/api/types").then(function success(response) {
      this.types = response.data;
    });

    this.navbar.active = localStorage.getItem("link");

  },
  computed: {
    numberOfProducts: function() {
      return this.total + this.loadedProducts.length;
    }
  },
  methods: {
    handleLink: function(link) {

      if(link === "goBack") {

        var lastPage = window.history.back();

        this.navbar.active = lastPage;
        localStorage.setItem("link", lastPage);

        return window.location = "/" + lastPage;

      }

      this.navbar.active = link;

      localStorage.setItem("link", link);

      return window.location = "/" + link;

    },
    handleModal: function(elements) {
      this.modal.elements = elements;
      this.modal.showModal = !this.modal.showModal;
    },
    handleMessages: function(message, type) {
      alertify.notify(message, type, 5);
    },
    loadMore: function() {

      this.loading = true;

      if(!this.allLoaded) {

        this.$http.get("/products/api/token").then(function success(response) {

          var csrf_name = response.data.token.name;
          var csrf_value = response.data.token.value;

          // Collect the form data
          var data = {
            index: this.index,
            chunk: 4,
            csrf_name: csrf_name,
            csrf_value: csrf_value
          }

          this.$http.post("/products/api/all", data).then(function success(response) {

            this.allLoaded = response.data.allLoaded;
            this.index = response.data.index;

            // Create a binding to this.loadedProducts to work inside the loop
            var loadedProducts = this.loadedProducts;

            response.data.payload.forEach(function(product) {

              loadedProducts.push(product);

            });

            // Scroll to last loaded items
            $('body').scrollTo(".load-more-button", 800, {
              easing: 'swing',
            });

            // Stop the loading icon
            this.loading = false;


          }, function error(response) {
            console.log(response);
          });

        });

      }

    }
  }
});
