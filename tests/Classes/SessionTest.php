<?php

use PHPUnit\Framework\Testcase;
use App\Classes\Session;

class SessionTest extends Testcase
{
  protected static $session;

  public static function setUpBeforeClass()
  {
    self::$session = new Session;
  }

  public function testItCanCreateSession()
  {
    self::$session->set("username", "John Doe");

    $this->assertEquals(self::$session->get("username"), "John Doe");
  }

  public function testItCanClearSession()
  {
    self::$session->clear("username");

    $this->assertFalse(self::$session->get("username"));
  }

  public static function teardownAfterClass()
  {
    self::$session->clear("username");
    self::$session = null;
  }

}
