<?php

use PHPUnit\Framework\Testcase;
use App\Classes\Helper;

class HelperTest extends Testcase
{
  public function testItCanSanitizeString()
  {
    $badString = "<?php var_dump('lol'); ?>";
    $badStringSanitized = Helper::sanitize($badString);

    $this->assertNotEquals($badString, $badStringSanitized);
  }

  public function testItCanSanitizeArray()
  {
    $badArray = [
      "username" => "<?php var_dump('lol'); ?>",
      "password" => 123
    ];

    $badArraySanitized = Helper::sanitize($badArray);

    $this->assertNotEquals($badArray["username"], $badArraySanitized["username"]);
    $this->assertEquals($badArray["password"], $badArraySanitized["password"]);

  }

}
