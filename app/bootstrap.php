<?php

require "../../vendor/autoload.php";

use Valitron\Validator;
use App\Classes\Database;
use App\Classes\Session;
use Slim\Views\Twig;
use Slim\Csrf\Guard;
use Slim\Views\TwigExtension;
use Slim\App;
use Slim\Container;
use App\Classes\NotFoundHandler;

// Start Sessions if necessary
if (session_status() == PHP_SESSION_NONE) {
    Session::start();
}

// Set the Validator Language
Validator::langDir("../../vendor/vlucas/valitron/lang/");
Validator::lang('pt-br');

// Set default timezone
date_default_timezone_set("Europe/Lisbon");

// Config for Slim
$config = [
  "displayErrorDetails" => true,
  "addContentLengthHeader" => false
];

// Instantiate Slim with config
$app = new App([
  "settings" => $config
]);

// Register CSRF with container
$container = $app->getContainer();
$container["csrf"] = function ($c) {
    return new Guard;
};

// Add the CSRF Package
$app->add(new Guard);

// Add twig templating system
$container["view"] = function($container) {

  $view = new Twig("../../src/public/templates", [
    "cache" => "../../src/public/cache",
    "strict_variables" => true,
    "debug" => true
  ]);

  $basePath = rtrim(str_ireplace("index.php", "", $container["request"]->getUri()->getBasePath()), "/");
  $view->addExtension(new TwigExtension($container["router"], $basePath));

  return $view;

};

// Override the default 404 error handler
$container["NotFoundHandler"] = function ($c) {
    return new NotFoundHandler($c->get("view"), function ($request, $response) use ($c) {
        return $c["response"]->withStatus(404);
    });
};

// Add Controllers to DI Container
$container["TestController"] = function ($container) {
    return new \App\Controllers\TestController($container);
};

$container["HomeController"] = function ($container) {
    return new \App\Controllers\HomeController($container);
};

$container["ProductController"] = function ($container) {
    return new \App\Controllers\ProductController($container);
};

$container["StatsController"] = function ($container) {
    return new \App\Controllers\StatsController($container);
};

// Set default title
$container["title"] = "Monthly";

// Connect to Database
$db = new Database;
$db->start();
