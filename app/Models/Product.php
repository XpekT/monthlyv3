<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Valitron\Validator;
use App\Classes\Helper;
use Stringy\StaticStringy as Stringy;
use App\Classes\Session;

class Product extends Eloquent
{
  protected $table        = "products";

  protected $fillable      = ["name", "price", "user", "type", "slug", "created_at"];

  protected static $types = [
    [ "name" => "Mercearia", "value"   => 1 ],
    [ "name" => "Lacticínios", "value" => 2 ],
    [ "name" => "Bebidas", "value"     => 3 ],
    [ "name" => "Frescos", "value"     => 4 ],
    [ "name" => "Beleza", "value"      => 5 ],
    [ "name" => "Congelados", "value"  => 6 ],
    [ "name" => "Bebé", "value"        => 7 ],
    [ "name" => "Higiene", "value"     => 8 ],
    [ "name" => "Limpeza", "value"     => 9 ],
    [ "name" => "Casa", "value"        => 10 ],
    [ "name" => "Animais", "value"     => 11 ],
    [ "name" => "Lazer", "value"       => 12 ],
    [ "name" => "Saúde", "value"       => 13 ],
    [ "name" => "Indefinido", "value"  => 14 ],
  ];

  public static function getTypes()
  {
    return self::$types;
  }

  public static function getTotal()
  {
    return self::count();
  }

  public static function search($data = [])
  {
    // Sanitize query
    $query = Helper::sanitize($data["query"]);
    $type  = Helper::sanitize($data["type"]);

    if($type == "normal") {

      $specific = false;

      $payload = self::join('types', 'products.type', '=', 'types.id')
                      ->where("products.name", "like", "%" . $query . "%")
                      ->orWhere("products.user", "like", "%" . $query . "%")
                      ->orWhere("types.name", "like", "%" . $query . "%")
                      ->select("products.id", "products.user", "products.name", "products.price", "products.slug", "products.created_at", "products.type", "types.name as section")
                      ->orderBy("products.created_at", "Desc")
                      ->get()
                      ->all();
    }

    if($type == "costier") {

      $specific = true;

      $payload = self::join('types', 'products.type', '=', 'types.id')
                      ->select("products.id", "products.user", "products.name", "products.price", "products.slug", "products.created_at", "products.type", "types.name as section")
                      ->orderBy("products.price", "Desc")
                      ->first();

    }

    if($type == "cheapest") {

      $specific = true;

      $payload = self::join('types', 'products.type', '=', 'types.id')
                      ->select("products.id", "products.user", "products.name", "products.price", "products.slug", "products.created_at", "products.type", "types.name as section")
                      ->orderBy("products.price", "Asc")
                      ->first();

    }

    if($type == "latest") {

      $specific = true;

      $payload = self::join('types', 'products.type', '=', 'types.id')
                      ->select("products.id", "products.user", "products.name", "products.price", "products.slug", "products.created_at", "products.type", "types.name as section")
                      ->orderBy("products.created_at", "Desc")
                      ->first();

    }

    if($type == "type") {

      $specific = false;

      $payload = self::join('types', 'products.type', '=', 'types.id')
                      ->where("products.type", $query)
                      ->select("products.id", "products.user", "products.name", "products.price", "products.slug", "products.created_at", "products.type", "types.name as section")
                      ->orderBy("products.created_at", "Desc")
                      ->get()
                      ->all();

    }

    return [
      "payload"  => $payload,
      "specific" => $specific
    ];
  }

  public static function getAllBySlug($slug)
  {
    if(is_array($slug)) {

      $slug = $slug["slug"];

    }

    return self::where("slug", Helper::sanitize($slug))
               ->join('types', 'products.type', '=', 'types.id')
               ->select("products.id", "products.user", "products.name", "products.price", "products.slug", "products.created_at", "products.type", "types.name as section")
               ->orderBy("created_at", "desc")
               ->get()
               ->all();
  }

  public static function getAll($data = [])
  {
    // Check if we only want all of the products as raw as possible
    if(isset($data["raw"])) {
      return self::join('types', 'products.type', '=', 'types.id')
                 ->select("products.id", "products.user", "products.name", "products.price", "products.slug", "products.created_at", "products.type", "types.name as section")
                 ->orderBy("created_at", "desc")
                 ->get()
                 ->all();
    }

    $totalProducts = self::count();
    $index = isset($data["index"]) ? $data["index"] : 0;
    $chunk = isset($data["chunk"]) ? $data["chunk"] : 8;

    if($index < $totalProducts && $index >= 0) {

      $payload = self::join('types', 'products.type', '=', 'types.id')
                 ->select("products.id", "products.user", "products.name", "products.price", "products.slug", "products.created_at", "products.type", "types.name as section")
                 ->skip($index)
                 ->take($chunk)
                 ->orderBy("created_at", "desc")
                 ->get();

      // Update index
      $index = $index + $chunk;

      return [
        "payload"       => $payload,
        "index"         => $index,
        "totalProducts" => $totalProducts,
        "allLoaded"     => false
      ];

    }

    return [
      "allLoaded" => true,
      "payload"   => []
    ];

  }

  public function addProduct($data = [])
  {
    // Sanitize Data
    $data = Helper::sanitize($data);

    // Validate Data
    $v = new Validator($data);

    // Rename the labels to PT
    $v->labels([
      "user"  => "Utilizador",
      "name"  => "Nome",
      "price" => "Preço",
      "type"  => "Tipo"
    ]);

    $v->rule("required", ["user", "name", "price", "type"]);
    $v->rule("numeric", "price");

    if(!$v->validate()) {
      return [
        "error" => true,
        "errorsMessageBag" => $v->errors()
      ];
    }

    // Add the product
    $this->user         = $data["user"];
    $this->name         = $data["name"];
    $this->price        = $data["price"];
    $this->type         = $data["type"];
    $this->slug         = Stringy::slugify(Stringy::htmlDecode($data["name"]));

    if(!$this->save()) {
      return [
        "error" => true,
        "message" => "De momento, não é possível concluir o seu pedido."
      ];
    }

    return [
      "success" => true,
      "message" => "Produto Adicionado!"
    ];

  }

  public function editProduct($data = [])
  {
    $form = Helper::sanitize($data["form"]);
    $id   = Helper::sanitize($data["id"]);

    // Check if product exists
    $product = $this->where("id", $id)->get()->first();

    if($product !== null) {

      $v = new Validator($form);

      // Rename the labels to PT
      $v->labels([
        "user"  => "Utilizador",
        "name"  => "Nome",
        "price" => "Preço",
        "type"  => "Tipo"
      ]);

      // Validate and update product
      if(isset($form["user"]) && mb_strlen($form["user"]) > 0) {

        $v->rule("alphaNum", ["user"]);
        $v->rule("lengthMin", ["user"], 3);

        $product->user = $form["user"];

      }

      if(isset($form["name"]) && mb_strlen($form["name"]) > 0) {

        $v->rule("lengthMin", ["name"], 3);

        $product->name = $form["name"];
        $product->slug = Stringy::slugify(Stringy::htmlDecode($form["name"]));

      }

      if(isset($form["type"]) && mb_strlen($form["type"]) > 0) {
        $product->type = $form["type"];
      }

      if(isset($form["price"]) && mb_strlen($form["price"]) > 0) {

        $v->rule("numeric", ["price"]);

        $product->price = $form["price"];

      }

      if(!$v->validate()) {
        return [
          "error"            => true,
          "errorsMessageBag" => $v->errors()
        ];
      }

      if($product->save()) {

        return [
          "success" => true,
          "message" => "Produto Actualizado!"
        ];

      }

      return [
        "error"   => true,
        "message" => "Ocorreu um erro! Por favor, tente mais tarde."
      ];

    }

    return [
      "error"   => true,
      "message" => "Produto não existe!"
    ];

  }

  public function deleteProduct($data = [])
  {
    // Check if the product exists
    $product = $this->find($data["id"]);

    if($product !== null) {

      if($product->delete()) {

        return [
          "success" => true,
          "message" => "Produto Eliminado!",
        ];

      }

      return [
        "error"   => true,
        "message" => "Ocorreu um erro! Por favor, tente mais tarde."
      ];

    }

    return [
      "error"   => true,
      "message" => "O produto não existe!"
    ];

  }

}
