<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;
use App\Models\Product;

class StatsController
{
  protected $container;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function index(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    return $this->container->view->render($res, "products/stats.html.twig", [
      "title" => $this->container->title . " - Estatísticas",
      "products" => Product::getAll(["raw" => true])
    ]);
  }
}
