<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;

class HomeController
{
  protected $container;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function index(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $token = [
      "name"  => $req->getAttribute("csrf_name"),
      "value" => $req->getAttribute("csrf_value"),
    ];

    return $this->container->view->render($res, "welcome.html.twig", [
      "title" => $this->container->title . " - Welcome",
      "token" => $token
    ]);
  }
}
