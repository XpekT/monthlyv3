<?php

namespace App\Controllers;

use App\Models\Test;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Container;

class TestController
{
  protected $container;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function get(Request $req, Response $res, $args)
  {
    $data = Test::all();

    return $this->container->view->render($res, "test.html.twig", [
      "data" => $data,
      "title" => "Test"
    ]);
  }
}
