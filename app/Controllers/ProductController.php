<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;
use App\Models\Product;
use App\Classes\Helper;
use Carbon\Carbon;
use Stringy\Stringy;

class ProductController
{
  protected $container;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function index(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $token = [
      "name"     => $req->getAttribute($this->container->csrf->getTokenNameKey()),
      "value"    => $req->getAttribute($this->container->csrf->getTokenValueKey())
    ];

    return $this->container->view->render($res, "products/add.html.twig", [
      "title" => $this->container->title . " - Novo Produto",
      "token" => $token,
      "types" => Product::getTypes()
    ]);
  }

  public function add(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $product = new Product;

    return $res->withJson($product->addProduct($req->getParsedBody()));
  }

  public function all(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $token = [
      "name"     => $req->getAttribute($this->container->csrf->getTokenNameKey()),
      "value"    => $req->getAttribute($this->container->csrf->getTokenValueKey())
    ];

    $products = Product::getAll();

    return $this->container->view->render($res, "products/all.html.twig", [
      "title"    => $this->container->title . " - Todos os Produtos",
      "token"    => $token,
      "products" => $products,
      "types"    => Product::getTypes()
    ]);
  }

  public function searchGET(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    return $this->container->view->render($res, "products/search.html.twig", [
      "title"    => $this->container->title . " - Procurar",
      "types"    => Product::getTypes()
    ]);

  }

  public function searchPOST(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    return $res->withJson(Product::search($req->getParsedBody()));
  }

  public function getOne(ServerRequestInterface $req, ResponseInterface $res, $args)
  {

    $products = Product::getAllBySlug($args["slug"]);

    if(!empty(array_filter($products))) {
      return $this->container->view->render($res, "products/single.html.twig", [
        "title"    => $this->container->title . " - Produto",
        "products" => $products,
        "stringy"  => new Stringy,
        "types"    => Product::getTypes()
      ]);
    }

    return $this->container->view->render($res->withStatus(404), "errors/404.html.twig", [
      "title" => $this->container->title . " - 404 - Not Found!"
    ]);

  }

  public function apiGET(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $request = $args["request"];
    $slug = isset($args["slug"]) ? $args["slug"] : NULL;

    if($request === "token") {

      $token = [
        "name"     => $req->getAttribute($this->container->csrf->getTokenNameKey()),
        "value"    => $req->getAttribute($this->container->csrf->getTokenValueKey())
      ];

      return $res->withJson([
        "token" => $token
      ]);

    }

    if($request === "types") {
      return $res->withJson(Product::getTypes());
    }

    if($request === "all") {
      return $res->withJson(Product::getAll($req->getAttribute("index")));
    }

    if($request === "total") {
      return $res->withJson(Product::getTotal());
    }

  }

  public function apiPOST(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $request = $args["request"];

    if($request === "all") {
      return $res->withJson(Product::getAll($req->getParsedBody()));
    }

    if($request === "allBySlug") {
      return $res->withJson(Product::getAllBySlug($req->getParsedBody()));
    }

  }

  public function edit(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $product = new Product;

    $data = [
      "form" => $req->getParsedBody(),
      "id"    => $args["id"],
    ];

    return $res->withJson($product->editProduct($data));

  }

  public function delete(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $product = new Product;

    $data = [
      "id"    => $args["id"],
    ];

    return $res->withJson($product->deleteProduct($data));

  }

}
