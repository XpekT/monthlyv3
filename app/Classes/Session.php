<?php

namespace app\classes;

class Session
{
  
  public static function start()
  {
    session_start();
    session_regenerate_id(true);
  }

  public static function set($name, $value = [])
  {
    return $_SESSION[$name] = $value;
  }

  public static function get($name)
  {
    if(isset($_SESSION[$name])) {
      return $_SESSION[$name];
    }

    return false;
  }

  public static function clear($name)
  {
    unset($_SESSION[$name]);
  }

}
