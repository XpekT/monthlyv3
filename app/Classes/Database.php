<?php

namespace App\Classes;

use Illuminate\Database\Capsule\Manager;

class Database
{
  private $db;

  public function __construct()
  {
    $this->db = new Manager();

    $this->db->addConnection([
      "driver" => "mysql",
      "host" => "127.0.0.1",
      "database" => "monthly",
      "username" => "root",
      "password" => "root",
      "charset" => "utf8",
      "collation" => "utf8_unicode_ci",
      "prefix" => ""
    ]);

    $this->db->setAsGlobal();

  }

  public function getInstance()
  {
    return $this->db;
  }

  public function start()
  {
    return $this->db->bootEloquent();
  }
}
