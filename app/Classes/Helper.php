<?php

namespace App\Classes;

class Helper
{

  public static function sanitize($data)
	{
    if(is_array($data)) {
      foreach ($data as $index => $value) {
        $data[$index] = htmlentities(trim($value), ENT_QUOTES, "UTF-8");
      }
    }

    if(is_string($data)) {
      $data = htmlentities(trim($data), ENT_QUOTES, "UTF-8");
    }

    return $data;
  }

}
