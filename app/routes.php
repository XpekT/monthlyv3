<?php

require "bootstrap.php";

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Home Route - Redirects to the Welcome Route
$app->get("/", function (Request $request, Response $response) {
  return $response->withHeader("Location", "welcome");
});

// Welcome Route
$app->get("/welcome", "HomeController:index")->setName("welcome");


// Products Route Group - CRUD
$app->group("/products", function() {

  // GET route for the page to insert new product
  $this->get("/new", "ProductController:index");

  // POST route to create a new product
  $this->post("/new", "ProductController:add");

  // GET route to select a given product
  $this->get("/product/{slug}", "ProductController:getOne");

  // GET route for all products on the DB
  $this->get("/all", "ProductController:all");

  // API
  $this->get("/api/{request}", "ProductController:apiGET");
  $this->post("/api/{request}", "ProductController:apiPOST");

  // POST route to edit a product
  $this->post("/product/edit/{id}", "ProductController:edit");

  // POST route to delete a product
  $this->post("/product/delete/{id}", "ProductController:delete");

  // GET route to display the products search page
  $this->get("/search", "ProductController:searchGET");

  // POST route to search for products
  $this->post("/search", "ProductController:searchPOST");

});

$app->get("/statistics", "StatsController:index");

// Test Route
$app->get("/test", "TestController:get");
